package qcouv.algorithms;

import cusack.hcg.games.weighted.WeightedInstance;
import cusack.hcg.graph.algorithm.AbstractAlgorithm;
import cusack.hcg.graph.algorithm.util.PermutationGenerator;

/**
 * Insert comment here about what your algorithm does.
 * 
 * @author Insert your name here.
 */
public class TravelingSalesman extends AbstractAlgorithm<WeightedInstance> {
	// You are encouraged to delete all of the non-Javadoc comments when
	// you have learned what you need from them.

	/*
	 * The superclass (AbstractAlgorithm) has several fields that you may use as
	 * needed. Each is listed below with a few comments. DO NOT UNCOMMENT THE
	 * CODE RELATED TO THESE FIELDS! The code is given so you know how they are
	 * declared in the superclass. If you uncomment the code you will most
	 * certainly break your algorithm.
	 */

	//protected WeightedInstance puzzle;
	//
	// The field puzzle contains all of the data related to a graph problem that
	// you will need for your algorithm. When Algoraph runs algorithms, puzzle
	// is automatically set for you.
	//
	// The actual data for a puzzle is stored in a GraphWithData object in the
	// puzzle object. However, delegate methods are provided in puzzle for
	// many of the methods you may want to use. Alternatively you can get
	// direct access to the GraphWithData object using puzzle.getGraph().
	// You can then use the methods getData and setData to access/modify
	// data associated with vertices.
	//
	// The GraphWithData object stores the structure of the graph in a field
	// of type Graph and the data in maps--one that maps Vertex to VertexData
	// and one that maps Edge to EdgeData. You can get the Graph object by
	// calling getGraph on the GraphWithData object, or by calling
	// getGraph().getGraph() from the puzzle.
	//
	// It should be noted that your algorithm does not have to operate on the
	// puzzle class. You can take the data from that class and store it however
	// you wish. In particular, you may want to store the graph using
	// EfficientListGraph or EfficientMatrixGraph. You may also want to store
	// the data in arrays or ArrayLists instead of in Maps.
	//
	// See the Javadoc comments for WeightedInstance for a complete list of
	// methods that you may want to use.

	 //protected int numberOfOperations = -1;
	//
	// If you override countsOperations() to return true, you are expected to
	// keep track of the number of operations your algorithm uses by modifying
	// the value of numberOfOperations. Unless otherwise specified, you may
	// interpret "number of operations" as appropriate for your problem.
	private int minCost;
	private int[] optimalTour;

	public TravelingSalesman() {
		// You must have a default constructor in this class, even if it does
		// nothing. In fact, it probably SHOULD do nothing. The puzzle isn't
		// set yet, so you don't know what to do with any data.
		minCost = Integer.MAX_VALUE; // no tour yet
		optimalTour = new int[] {}; // no tour yet
	}

	/**
	 * This method is called first, and allows you to initialize any extra data
	 * that you may need in your algorithm.
	 */
	@Override
	public void initializeData() {
		// Here is where you should initialize any data, including copying the
		// graph/data if you plan on using an alternative representation.
	}

	/**
	 * Runs the Algorithm. This is the most important method.
	 */
	@Override
	public void runAlgorithm() {
		// Implement your algorithm here!
		int[][] A = puzzle.getAdjacencyMatrix(); // creates adjacency matrix of the puzzle (graph)
		
		// create a permutation generator for the puzzle (0...n-1)
		PermutationGenerator pg = new PermutationGenerator(puzzle.getNumberOfVertices());
		
		//iterate through each permutation
		while(pg.hasNext()) {
			int currentPermutationCost = 0; // not yet found
			int[] currentPermutation = pg.next();
			
			// iterate through the current permutation to calculate the cost
			for(int i = 0; i< currentPermutation.length - 1; i++) {
				int from = currentPermutation[i];
				int to = currentPermutation[i+1];
				currentPermutationCost += A[from][to];
			}
			// add on cost from last vertex in permutation to first vertex
			int firstVertex = currentPermutation[0];
			int lastVertex = currentPermutation[currentPermutation.length - 1];
			currentPermutationCost += A[lastVertex][firstVertex];
			
			// if the cost of the current permutation is less than the minCost, update minCost and optimalTour
			if(currentPermutationCost < minCost) {
				minCost = currentPermutationCost;
				optimalTour = currentPermutation;
			}
		}
		numberOfOperations = minCost; // best permutation found gives us our optimal tour
	}

	/**
	 * HTML text that will be displayed in the Algorithm Runner that explains
	 * what the algorithm is doing. Think of the status as a progress report.
	 * 
	 * @return
	 */
	@Override
	public String getProgressReport() {
		return minCost + ":" + optimalTour;
	}

	/**
	 * Return the result of the algorithm in the context of the problem. i.e.
	 * Solvable, Unsolvable, Unknown. This tells more than "Done" or "Not_Done"
	 * that is given in the more general sense
	 * 
	 * @return the result
	 */
	@Override
	public String getResult() {
		String result = minCost + ":";
		for (int i = 0; i<optimalTour.length; i++) {
			result += optimalTour[i] + " ";
		}
		return result;
	}

	/**
	 * @return A string representation of the current state of the problem data.
	 *         This should be in the same format as it was passed in to
	 *         setProblemData, only updated to give the current state based on
	 *         the algorithm. The idea is that this can be used to show the
	 *         progress of the algorithm by some GUI (or in text form, I
	 *         suppose).
	 */
	@Override
	public String getCurrentProblemData() {
		// You must return a string in the exact same format as the method
		// currentPuzzleDataToString in WeightedInstance uses.
		// If you are using the puzzle field, you can probably just do:
		// return puzzle.currentPuzzleDataToString();
		return puzzle.currentPuzzleDataToString();
	}

	/**
	 * @return True if and only if the algorithm intends to keep track of
	 *         operations.
	 */
	@Override
	public boolean countsOperations() {
		// Leave this alone if you don't intend to count operations.
		// Change it to "return true" if you do, and then make sure
		// you actually do count operations.
		// Recall that AbstractAlgorithm (the superclass of this class)
		// has field numberOfOperations that you can update as needed.
		return true;
	}

	/**
	 * Mostly for use in comparing results from previous versions of algorithms,
	 * and/or so we can know whether or not data in the database is based on an
	 * older version of the algorithm. Start with "return 1.0". Go up by
	 * whatever increments you like (e.g. 1.1, 1.2, etc. or 1, 2, 3, etc.)
	 * 
	 * @return
	 */
	@Override
	public double getVersion() {
		return 1;
	}

	/**
	 * This should return the class object representing the subclass of
	 * WeightedInstance that this algorithm runs on. If you change what subclass
	 * of WeightedInstance your algorithm is applicable for, those changes must be
	 * reflected in the return type of this method, the Class object that is
	 * returned, and the parameter in the class signature.
	 * 
	 * @return The class object representing the subclass of WeightedInstance that
	 *         this algorithm runs on.
	 */
	@Override
	public Class<WeightedInstance> getProblemType() {
		return WeightedInstance.class;
	}
	
	/**
	 * (Hopefully) gracefully stop the computation.
	 */
	@Override
	public void quit() {
	}
	@Override
	public String argumentFormat() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void parseArguments(String arg0) throws RuntimeException {
		// TODO Auto-generated method stub
	}
	
}